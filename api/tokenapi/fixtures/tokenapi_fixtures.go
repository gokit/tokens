package fixtures

import (
	"encoding/json"

	"github.com/gokit/tokens"
)

// json fixtures ...
var (
	TokenJSON = `{


    "value":	"f9r5whlmy8d921p0wdx7",

    "public_id":	"kzh046l7yq3kj6x4dq9c5ezenhf9wo",

    "target_id":	"k97q63vuz385brafw259"

}`

	TokenCreateJSON = `{


    "target_id":	"qn9yiy7b2ertiqjqmux6",

    "value":	"trzrxtnymwxeqlhvdknx",

    "public_id":	"ryhkktq7zvg48bkuqnrcjo9v4wg35b"

}`

	TokenUpdateJSON = `{


    "target_id":	"nv4qfo8irk7fbc5jfui3",

    "value":	"pw5nzvkpr570xus2al30",

    "public_id":	"d82a7fbcjygkxoefz1iedd9drikidy"

}`
)

// LoadCreateJSON returns a new instance of a tokens.Token.
func LoadCreateJSON(content string) (tokens.Token, error) {
	var elem tokens.Token

	if err := json.Unmarshal([]byte(content), &elem); err != nil {
		return tokens.Token{}, err
	}

	return elem, nil
}

// LoadUpdateJSON returns a new instance of a tokens.Token.
func LoadUpdateJSON(content string) (tokens.Token, error) {
	var elem tokens.Token

	if err := json.Unmarshal([]byte(content), &elem); err != nil {
		return tokens.Token{}, err
	}

	return elem, nil
}

// LoadTokenJSON returns a new instance of a tokens.Token.
func LoadTokenJSON(content string) (tokens.Token, error) {
	var elem tokens.Token

	if err := json.Unmarshal([]byte(content), &elem); err != nil {
		return tokens.Token{}, err
	}

	return elem, nil
}
