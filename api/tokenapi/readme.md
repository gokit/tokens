Token HTTP API 
===============================

[![Go Report Card](https://goreportcard.com/badge/github.com/gokit/tokens/api/tokenapi)](https://goreportcard.com/report/github.com/gokit/tokens/api/tokenapi)

Token HTTP API is a auto generated http api for the struct `Token`.

The API expects the user to implement and provide the backend interface to provided underline db interaction:

```go
type Backend interface{
    Delete(context.Context, string) error
    Get(context.Context, string) (tokens.Token, error)
    Update(context.Context, string, tokens.Token) error
    GetAll(context.Context, string, string, int, int) ([]tokens.Token, int, error)
    Create(context.Context, tokens.Token) (tokens.Token, error)
}
```

It exposes the following methods for each endpoint:

## Create { POST /Token/ }
### Method: `func (api *HTTPAPI) Create(ctx *httputil.Context) error`

Create receives the provided record of the Token type which is delieved the 
JSON content to the HTTP API. This will in turn return a respective status code.

- Expected Request Body

```http
    Content-Type: application/json
```

```json
{


    "public_id":	"b1lpylo1p7zpk3qmfv06betx3lbcfh",

    "target_id":	"cqu72mlbsxmmignx8v9c",

    "value":	"ubldnuup4ao3utbwyyb9"

}
```

- Expected Status Code

```
Failure: 500
Success: 201
```

- Expected Response Body

```http
    Content-Type: application/json
```

```json
{


    "value":	"h78phoi9lgqx3an1vurv",

    "public_id":	"bzkepok1x1wwalmr5q4liazrylb9n8",

    "target_id":	"vwjprv9est13gbcifrdc"

}
```

## INFO /Token/
### Method: `func (api *HTTPAPI) Info(ctx *httputil.Context) error`

Info returns total of records available in api for type tokens.Token.

- Expected Status Code

```
Failure: 500
Success: 200
```

- Expected Response Body

```http
    Content-Type: application/json
```

```json
{
    "total": 10,
}
```

## GET /Token/:public_id
### Method: `func (api *HTTPAPI) Get(ctx *httputil.Context) error`

Get retrives a giving record of the Token type from the HTTP API returning received result as a JSON
response. It uses the provided `:public_id` parameter as the paramter to identify the record.

- Expected Request Parameters

```
    :public_id
```

- Expected Status Code

```
Failure: 500
Success: 200
```

- Expected Response Body

```http
    Content-Type: application/json
```

```json
{


    "value":	"f7u87wkof448u7gc3qzg",

    "public_id":	"rtss6r6tl614iauw8tdb4ohu1aindj",

    "target_id":	"7je441hwlc39hqocnrlj"

}
```

## GET /Token/
### Method: `func (api *HTTPAPI) GetAll(ctx *httputil.Context) error`

Get retrives all records of the Token type from the HTTP API.

- Expected Status Code

```
Failure: 500
Success: 200
```

- Expected Response Body

```http
    Content-Type: application/json
```

```json
[{


    "value":	"cx5hnyb7zdpy9hvirpg5",

    "public_id":	"1choowqid2q47m4ejhi58f1tzdaqwn",

    "target_id":	"uve2wibhmt9wx9iwll8a"

},{


    "value":	"cr090dx2cup7m538dcdx",

    "public_id":	"y1zzb04lfpxhhsq11a3djsikbfkr5b",

    "target_id":	"1dhqq8ayl92ivtt3kjkp"

}]
```

## PUT /Token/:public_id
### Method: `func (api *HTTPAPI) Update(ctx *httputil.Context) error`

Update attempts to update a giving record of the Token type from the HTTP API returning received result as a JSON
response. It uses the provided `:public_id` parameter as the paramter to identify the record with the provided JSON request body.

- Expected Request Parameters

```
    :public_id
```

- Expected Request Body

```http
    Content-Type: application/json
```

```json
{


    "value":	"tuzkuh1axye7g84t12ia",

    "public_id":	"2jdehf87xl5qrv8tk4nelvrmfyv6rb",

    "target_id":	"19v469p7s7n1185q3zqu"

}
```

- Expected Status Code

```
Failure: 500
Success: 204
```

## DELETE /Token/:public_id
### Method: `func (api *HTTPAPI) Delete(ctx *httputil.Context) error`

Get deletes a giving record of the Token type from the HTTP API returning received result as a JSON
response. It uses the provided `:public_id` parameter as the paramter to identify the record.

- Expected Request Parameters

```
    :public_id
```

- Expected Status Code

```
Failure: 500
Success: 204
```

