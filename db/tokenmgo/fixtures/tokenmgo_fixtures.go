package fixtures

import (
     "encoding/json"


     "github.com/gokit/tokens"

)


// json fixtures ...
var (
 TokenJSON = `{


    "target_id":	"my9001bidn9ve82d6ql2",

    "value":	"1hcwe93xsmfvlka0mmt7",

    "public_id":	"w7a6igvvbjypxp1yvaw8liw4jx3n8q"

}`
)

// LoadTokenJSON returns a new instance of a tokens.Token.
func LoadTokenJSON(content string) (tokens.Token, error) {
	var elem tokens.Token

	if err := json.Unmarshal([]byte(content), &elem); err != nil {
		return tokens.Token{}, err
	}

	return elem, nil
}

