package fixtures

import (
     "encoding/json"


     "github.com/gokit/tokens"

)


// json fixtures ...
var (
 TokenJSON = `{


    "value":	"ybx21w5retc3hvrid0xi",

    "public_id":	"ly0ucql5tiwzwagaq33dgw3z39n1mm",

    "target_id":	"ayxnz95pp310n4cd50wk"

}`
)

// LoadTokenJSON returns a new instance of a tokens.Token.
func LoadTokenJSON(content string) (tokens.Token, error) {
	var elem tokens.Token

	if err := json.Unmarshal([]byte(content), &elem); err != nil {
		return tokens.Token{}, err
	}

	return elem, nil
}

