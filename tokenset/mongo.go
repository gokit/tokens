package tokenset

import (
	"context"
	"fmt"

	"github.com/gokit/tokens"
	"github.com/gokit/tokens/db/tokenmgo"
	"github.com/influx6/faux/metrics"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// TokensetMGO returns a new instance of tokensetMgo which has it's index set and
// giving collection named {{domain}}_tokenset_collection.
func TokensetMGO(domain string, m metrics.Metrics, mo tokenmgo.Mongod) tokens.TokenSet {
	domainName := fmt.Sprintf("%s_tokenset_collection", domain)
	mgoDB := tokenmgo.New(domainName, m, mo, mgo.Index{
		Unique: true,
		Key:    []string{"value"},
		Name:   "value_index",
	})

	return &tokensetMgo{db: mgoDB}
}

// tokensetMgo implements the tokenset.Set interface on top of
// mongodb.
type tokensetMgo struct {
	db *tokenmgo.TokenDB
}

// Has returns true/false if giving token exists or not within underline
// db. Returns an error if call to db failed.
func (tf *tokensetMgo) Has(ctx context.Context, targetID string, token string) (bool, error) {
	var status bool
	err := tf.db.Exec(ctx, func(col *mgo.Collection) error {
		query := bson.M{"target_id": targetID, "value": token}
		total, err := col.Find(query).Count()
		if err != nil {
			return err
		}
		if total != 0 {
			status = true
		}
		return nil
	})

	return status, err
}

// Remove removes record with target id and token from db. Returns an error if call to db failed.
func (tf *tokensetMgo) Remove(ctx context.Context, targetID string, token string) error {
	return tf.db.Exec(ctx, func(col *mgo.Collection) error {
		query := bson.M{"target_id": targetID, "value": token}
		return col.Remove(query)
	})
}

// Add adds giving underline tokendb into db, returning error if
// it fails to do so, or call to db errors out.
func (tf *tokensetMgo) Add(ctx context.Context, targetID string, token string) (tokens.Token, error) {
	tokened := tokens.NewToken(targetID, token)
	return tokened, tf.db.Create(ctx, tokened)
}
