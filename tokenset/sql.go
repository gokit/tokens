package tokenset

import (
	"context"
	"fmt"

	"github.com/gokit/tokens"
	"github.com/gokit/tokens/db/tokensql"
	"github.com/influx6/faux/db/sql"
	"github.com/influx6/faux/db/sql/tables"
	"github.com/influx6/faux/metrics"
)

// TokensetSQL returns a new instance of SetMgoDB which has it's index set and
// giving collection named {{domain}}_tokenset.
func TokensetSQL(domain string, m metrics.Metrics, db sql.DB) tokens.TokenSet {
	domainName := fmt.Sprintf("%s_token_set", domain)
	tsql := tokensql.New(domainName, m, db, tables.TableMigration{
		TableName: domainName,
		Fields: []tables.FieldMigration{
			{
				FieldName:     "id",
				FieldType:     "integer",
				AutoIncrement: true,
				PrimaryKey:    true,
				NotNull:       true,
			},
			{
				NotNull:   true,
				FieldName: "value",
				FieldType: "varchar(255)",
			},
			{
				NotNull:   true,
				FieldName: "public_id",
				FieldType: "varchar(255)",
			},
			{
				NotNull:   true,
				FieldName: "target_id",
				FieldType: "varchar(255)",
			},
		},
	})

	return &tokensetSql{
		table: domainName,
		db:    tsql,
	}
}

type tokensetSql struct {
	table string
	db    *tokensql.TokenDB
}

// Has returns true/false if giving token exists or not within underline
// db. Returns an error if call to db failed.
func (tf *tokensetSql) Has(ctx context.Context, targetID string, token string) (bool, error) {
	var status bool
	err := tf.db.Exec(ctx, func(qx *sql.SQL, dx sql.DB) error {
		db, err := dx.New()
		if err != nil {
			return err
		}

		query := fmt.Sprintf("SELECT * FROM %s WHERE value=%+q target_id=%+q", tf.table, targetID, token)
		row := db.QueryRowx(query)
		if err := row.Err(); err != nil {
			return err
		}

		res, err := row.SliceScan()
		if err != nil {
			return err
		}

		if len(res) != 0 {
			status = true
		}
		return nil
	})
	return status, err
}

// Remove destroys an existing token record if it exists.
func (tf *tokensetSql) Remove(ctx context.Context, targetID string, token string) error {
	return tf.db.Exec(ctx, func(qx *sql.SQL, dx sql.DB) error {
		db, err := dx.New()
		if err != nil {
			return err
		}

		query := fmt.Sprintf("DELETE FROM %s WHERE value=%+q target_id=%+q", tf.table, targetID, token)
		row := db.QueryRowx(query)
		if err := row.Err(); err != nil {
			return err
		}

		return nil
	})
}

// Add adds giving underline tokendb into db, returning error if
// it fails to do so, or call to db errors out.
func (tf *tokensetSql) Add(ctx context.Context, targetID string, token string) (tokens.Token, error) {
	tokened := tokens.NewToken(targetID, token)
	return tokened, tf.db.Create(ctx, tokened)
}
